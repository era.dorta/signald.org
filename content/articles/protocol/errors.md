---
title: Errors
---

Errors can happen in response to a specific request, or while subscribed to incoming
messages. For example, when attempting to register without a valid captcha token,
an error response like this will be sent to the client:

```json
{
  "type": "register",
  "error": {
    "more": "https://signald.org/articles/captcha/",
    "message": "a captcha token is required to register"
  },
  "error_type": "CaptchaRequiredError"
}
```

The `error_type` field indicates which type the `error` field is ([`CaptchaRequiredError`](https://signald.org/protocol/structures/v1/CaptchaRequiredError/)
in this case).

clients should have mechanisms to deal with unexpected error types. changes to signald may cause future versions to give different errors.