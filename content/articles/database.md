# Database Usage

signald stores most of it's data in a database. Note that it does not store all data in the database
currently. Two databases options are available: sqlite and postgresql.

## sqlite

The default database, does not require any special setup. Data will be stored at `~/.config/signald/signald.db`
by default.

## Postgres

To use a postgres database, pass the `postgresql:` URL to signald the command line flag `--database`
or environment variable `SIGNALD_DATABASE`. For example:

```
SIGNALD_DATABASE=postgresql://postgres:password@my.database.host:5432/signald
```

For more details on available connect options, see
[the PostgreSQL JDBC connect documentation](https://jdbc.postgresql.org/documentation/head/connect.html).


## Moving from sqlite to Postgres

Before moving from sqlite to postgres, you must have migration 11 applied to your sqlite database
(introduced in [5101c6ef](https://gitlab.com/signald/signald/-/commit/5101c6ef3ad511f2ab6ea1515e217234f7eb888b)).

To move the data, run [`signaldctl move-db`](/signaldctl/reference/signaldctl_db-move/). After the data is moved,
the old sqlite file will be removed.

The docker container will run this automatically if `SIGNALD_DATABASE` is set to a postgresql URL and a sqlite file
is found.

