* on [Debian](/articles/install/debian/)
* with [Docker](/articles/install/docker/)
* from [source](/articles/install/source/)