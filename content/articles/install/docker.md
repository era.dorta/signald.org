---
title: Docker
---

Container images are mirroed to two registries:
* [docker hub](https://hub.docker.com/r/signald/signald): `docker.io/signald/signald`
* GitLab's container registry: `registry.gitlab.com/signald/signald`

| Tag        | Description |
|------------|-------------|
| `latest`   | The latest release build |
| `unstable` | Latest commit, updated automatically by CI |
| `0.18.0`   | All releases are tagged with the release version |

The image is built for `amd64`, `armv7` and `armv8`. Additional architectures may be available upon request.

The images run as user 1337 (as of signald 0.18.0, see note about migrating from previous verisons below)

signald stores both account state files and the unix socket file in `/signald`. Mount it as a volume:

```bash
docker run -v $(pwd)/run:/signald signald/signald
```

## Additional tools

[signaldctl](/signaldctl/) is available in the container and can be used with `docker exec`.

signaldctl can also be installed outside the container, but needs to know where the `/signald` volume
is mounted on the host. That can be configured with `signaldctl config set socketpath /path/to/volume/signald.sock`

## Docker for Mac

_Docker for Mac_ does not pass correctly sockets via Docker volumes to the host. [This Docker issue](https://github.com/docker/for-mac/issues/483#issuecomment-758836836)
lays it out in all the gory details. Attempts to connect to the mounted socket result in `Connection Refused` errors. Connections from other docker containers should work.

## Migrating from versions before 0.18.0

Prior to 0.18.0 the signald container image used the root user, which is not recommended for security reasons. This was fixed in the 0.18.0 release which will start as
root, fix permissions on the volume, then drop to the non-root user and start signald. Future images (0.19.0+) will start as the non-root user, so if you're upgrading
make sure to run 0.18.0 at least once.

A special tag, `0.18.0-non-root`, will be published. it starts as the non-root user and does not fix permissions on the volume.

In 0.18.1, a fix was added to skip trying to fix permissions and drop privileges if the container is started as non-root by some other means.