---
title: Configuration Options
---

signald has several configuration options to alter it's behavior. Most can be configured via the command line or an environment variable.

```
Usage: signald [-hmuv] [--dump-protocol] [--log-database-transactions]
               [--log-http-requests] [--system-socket]
               [--trust-all-keys-on-start] [--trust-new-keys]
               [-d=<dataPath>] [--database=<db>]
               [--decrypt-timeout=<decryptionTimeout>]
               [--metrics-http-port=port] [-s=<socketPath>]
  -d, --data=<dataPath>     Data storage location
      --database=<db>       jdbc connection string. Defaults to jdbc:sqlite:~/.
                              config/signald/signald.db. Only sqlite is
                              supported at this time.
      --decrypt-timeout=<decryptionTimeout>
                            decryption timeout (in seconds). if signald detects
                              that decryption has taken longer than this, it
                              will exit with code 101
      --dump-protocol       print a machine-readable description of the client
                              protocol to stdout and exit (https://signald.
                              org/articles/protocol/documentation/)
  -h, --help                display this help message
      --log-database-transactions
                            log when DB transactions occur and how long they
                              took. Note that db logs are at the debug level,
                              so --verbose should also be used. (env
                              SIGNALD_LOG_DB_TRANSACTIONS=true)
      --log-http-requests   log all requests send to the server. this is used
                              for debugging but generally should not be used
                              otherwise (env SIGNALD_HTTP_LOGGING=true)
  -m, --metrics             record and expose metrics in prometheus format (env
                              SIGNALD_ENABLE_METRICS)
      --metrics-http-port=port
                            metrics http listener port (env
                              SIGNALD_METRICS_PORT)
  -s, --socket=<socketPath> The path to the socket file
      --system-socket       make the socket file accessible system-wide
      --trust-all-keys-on-start
                            mark all known keys as trusted on startup (env
                              SIGNALD_TRUST_ALL_KEYS=true)
      --trust-new-keys      when a remote key changes, set trust level to
                              TRUSTED_UNVERIFIED instead of UNTRUSTED (env
                              SIGNALD_TRUST_NEW_KEYS=true)
  -u, --user-socket         put the socket in the user runtime directory
                              ($XDG_RUNTIME_DIR), the default unless --socket
                              or --system-socket is specified
  -v, --verbose             Verbose mode. Helpful for troubleshooting (env
                              SIGNALD_VERBOSE_LOGGING)

```