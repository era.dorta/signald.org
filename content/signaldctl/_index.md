---
title: signaldctl
---
*a cli client for [signald](https://gitlab.com/signald/signald)*

## Install

If you installed signald from [the debian repo](/articles/install/debian/), signaldctl probably got installed too. If not, it can be installed like any other package:

```
sudo apt install signaldctl
```

For those not using the debian repository, the latest build of signaldctl can be downloaded for your platform from the CI build:
* [Linux amd64](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl?job=build%3Ax86)
* [Linux arm](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl-linux-arm?job=build%3Across-compile%3A%20%5Blinux%2C%20arm%5D)
* [Linux arm64](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl?job=build%3Aaarch64)
* [Mac amd64](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl-darwin-amd64?job=build%3Across-compile%3A%20%5Bdarwin%2C%20amd64%5D)

Or build it yourself:

```
go get gitlab.com/signald/signald-go/cmd/signaldctl
```


*signaldctl is available in the [docker images](/articles/install/docker/)*